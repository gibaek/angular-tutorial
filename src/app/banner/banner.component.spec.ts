import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BannerComponent} from './banner.component';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';

fdescribe('BannerComponent', () => {
    let component: BannerComponent;
    let fixture: ComponentFixture<BannerComponent>;
    let de: DebugElement;
    let el: HTMLElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BannerComponent] // declare the test component
        })
            .compileComponents(); // compile template and css
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BannerComponent);
        component = fixture.componentInstance; // BannerComponent test instance
        fixture.detectChanges();

        // query for the title <h1> by CSS element selector
        de = fixture.debugElement.query(By.css('h1'));
        el = de.nativeElement;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should display original title', () => {
        expect(el.textContent).toContain(component.title);
    });

    it('should display a different test title', () => {
        component.title = 'Test Title';
        fixture.detectChanges();
        expect(el.textContent).toContain('Test Title');
    });


});
