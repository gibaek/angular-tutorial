import {NgModule} from '@angular/core';
import {DashboardComponent} from './dashboard.component';
import {DashboardHeroComponent} from './dashboard-hero.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {HeroSearchComponent} from '../hero/hero-search/hero-search.component';

const routes: Routes = [
    {path: 'dashboard', component: DashboardComponent}
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule
    ],
    declarations: [DashboardComponent, DashboardHeroComponent, HeroSearchComponent]
})
export class DashboardModule {
}
