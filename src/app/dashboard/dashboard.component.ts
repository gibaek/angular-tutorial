import {Component, OnInit} from '@angular/core';
import {Hero} from '../model/hero';
import {HeroService} from '../model/hero.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    heroes: Hero[] = [];

    constructor(private heroService: HeroService,
                private router: Router) {
    }

    ngOnInit(): void {
        this.heroService.getHeroes().subscribe(heroes => this.heroes = heroes.slice(1, 5));
    }

    gotoDetail(hero: Hero) {
        const url = `/detail/${hero.id}`;
        this.router.navigateByUrl(url);
    }

    get title() {
        const cnt = this.heroes.length;
        return cnt === 0 ? 'No Heroes' :
            cnt === 1 ? 'Top Hero' : `Top ${cnt} Heroes`;
    }


}
