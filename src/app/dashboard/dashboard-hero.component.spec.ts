import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DashboardHeroComponent} from './dashboard-hero.component';
import {DebugElement} from '@angular/core';
import {Hero} from '../model/hero';
import {By} from '@angular/platform-browser';
import {click} from '../../testing/index';

fdescribe('DashboardHeroComponent', () => {
    let component: DashboardHeroComponent;
    let fixture: ComponentFixture<DashboardHeroComponent>;
    let heroEl: DebugElement;
    let expectedHero: Hero;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardHeroComponent],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardHeroComponent);
        component = fixture.componentInstance;

        heroEl = fixture.debugElement.query(By.css('.hero'));

        // pretend that it was wired to something that supplied a hero
        expectedHero = new Hero(42, 'Test Name');
        component.hero = expectedHero;

        fixture.detectChanges(); // trigger initial data binding
    });

    it('should dispaly hero name', () => {
        const expectedPipedName = expectedHero.name.toUpperCase();
        expect(heroEl.nativeElement.textContent).toContain(expectedPipedName);
    });

    it('should raise selected event when click', () => {
        let selectedHero: Hero;
        component.selected.subscribe((hero: Hero) => selectedHero = hero);

        // heroEl.triggerEventHandler('click', null);
        click(heroEl);
        expect(selectedHero).toBe(expectedHero);
    });


});


//////////////////

fdescribe('DashboardHeroComponent when inside a test host', () => {
    let testHost: TestHostComponent;
    let fixture: ComponentFixture<TestHostComponent>;
    let heroEl: DebugElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DashboardHeroComponent, TestHostComponent], // declare both
        }).compileComponents();
    }));

    beforeEach(() => {
        // create TestHostComponent instead of DashboardHeroComponent
        fixture = TestBed.createComponent(TestHostComponent);
        testHost = fixture.componentInstance;
        heroEl = fixture.debugElement.query(By.css('.hero')); // find hero
        fixture.detectChanges(); // trigger initial data binding
    });
    it('should display hero name', () => {
        const expectedPipedName = testHost.hero.name.toUpperCase();
        expect(heroEl.nativeElement.textContent).toContain(expectedPipedName);
    });

    it('should raise selected event when clicked', () => {
        click(heroEl);
        // selected hero should be the same data bound hero
        expect(testHost.selectedHero).toBe(testHost.hero);
    });
});


////// Test Host Component //////
import {Component} from '@angular/core';

@Component({
    template: `
        <app-dashboard-hero [hero]="hero" (selected)="onSelected($event)"></app-dashboard-hero>`
})
class TestHostComponent {
    hero = new Hero(42, 'Test Name');
    selectedHero: Hero;

    onSelected(hero: Hero) {
        this.selectedHero = hero;
    }
}
