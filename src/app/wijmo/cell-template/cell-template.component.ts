import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {DataService} from '../data.service';
import * as wjcCore from 'wijmo/wijmo';
import * as wjcGrid from 'wijmo/wijmo.grid';

@Component({
    selector: 'app-cell-template',
    templateUrl: './cell-template.component.html',
    styleUrls: ['./cell-template.component.css']
})
export class CellTemplateComponent implements AfterViewInit {

    countries = 'US,Germany,UK,Japan,Italy,Greece'.split(',');
    data1: wjcCore.CollectionView;

    customTopLeft = true;
    customRowHeader = true;
    customRowHeaderEdit = true;
    customCell = true;
    customCellEdit = true;
    customColumnHeader = true;
    customGroupHeader = true;
    customGroup = true;
    customColumnFooter = true;
    customBottomLeft = true;

    uiCtx = {
        highlightDownloads: true
    };


    @ViewChild('flex1') flex1: wjcGrid.FlexGrid;
    constructor(private dataSvc: DataService) {
        const data = dataSvc.getAnyData();
        this.data1 = dataSvc.getCv(data);
    }

    ngAfterViewInit() {
        if (this.flex1) {
            this.flex1.columnFooters.rows.push(new wjcGrid.GroupRow());
            this.flex1.collapseGroupsToLevel(0);
        }
    }
}
