import {Component, OnInit, ViewChild} from '@angular/core';
import * as wjcCore from 'wijmo/wijmo';
import * as wjcGrid from 'wijmo/wijmo.grid';
import {DataService} from '../data.service';

@Component({
    selector: 'app-dynamic-column',
    templateUrl: './dynamic-column.component.html',
    styleUrls: ['./dynamic-column.component.css']
})

export class DynamicColumnComponent implements OnInit {

    data: wjcCore.CollectionView;
    columnsAvailable: wjcCore.CollectionView = new wjcCore.CollectionView();
    columns: wjcCore.CollectionView;
    columnLayout: any[];


    @ViewChild('flex') flex: wjcGrid.FlexGrid;

    constructor(private dataSvc: DataService) {
        const data = dataSvc.getExampleData(10);
        this.data = new wjcCore.CollectionView(data);

        // build list of columns available
        const item = data[0];
        console.log('item: ');
        console.log(item);

        // const fields = [];
        const fields = new wjcCore.ObservableArray();

        if (localStorage) {
            if (localStorage['dynamic-columns']) {
                this.columnLayout = JSON.parse(localStorage['dynamic-columns'])['columns'];
                console.log(this.columnLayout);
                this.columns = new wjcCore.CollectionView(this.columnLayout);
            } else {
                /*기본 컬럼 레이아웃*/
            }
        }
    }

    ngOnInit(): void {
    }

    initGrid() {
        this.loadSort();
    }

    fixColumn(flex: wjcGrid.FlexGrid) {
        if (flex) {
            console.log(flex.columnLayout);
            const frozenCount = flex.frozenRows === 0 ? 2 : 0;
            flex.frozenColumns = frozenCount;
        }
    }

    loadSort() {
        if (localStorage.sort) {
            const sortStates = JSON.parse(localStorage.sort);
            for (const sortState of sortStates) {
                console.log('load sort ' + sortState.property + ', ' + sortState.ascending);
                const sd = new wjcCore.SortDescription(sortState.property, sortState.ascending);
                this.flex.collectionView.sortDescriptions.push(sd);
            }
        }
    }

    saveSort() {
        const sortStates: any[] = [];
        const description = this.flex.collectionView.sortDescriptions[0];
        sortStates.push({
            property: description.property,
            ascending: description.ascending,
        });
        console.log('** Saved Sort Sate: ' + JSON.stringify(sortStates));
        localStorage['sort'] = JSON.stringify(sortStates);
    }

    saveColumnLayout(s: wjcGrid.FlexGrid) {
        if (localStorage) {
            localStorage['dynamic-columns'] = this.flex.columnLayout;
            console.log(this.flex.columnLayout);
        }
    }

    // move item from columnsAvailable to columns
    addColumn() {
        const item = this.columnsAvailable.currentItem,
            index = this.columns.currentPosition;
        if (item) {
            this.columnsAvailable.remove(item);
            this.columns.sourceCollection.splice(Math.max(0, index), 0, item);
            this.columns.moveCurrentTo(item);
        }
    }

    // move item from columns to columnsAvailable
    removeColumn() {
        const item = this.columns.currentItem,
            index = this.columnsAvailable.currentPosition;
        if (item) {
            this.columns.remove(item);
            this.columnsAvailable.sourceCollection.splice(Math.max(0, index), 0, item);
            this.columnsAvailable.moveCurrentTo(item);
        }
    }

    // move a column within the columns collection
    moveColumn(offset) {
        const item = this.columns.currentItem;
        if (item) {
            const arr = this.columns.sourceCollection,
                index = arr.indexOf(item),
                newIndex = index + offset;
            if (index > -1 && newIndex > -1) {
                arr.splice(index, 1);
                arr.splice(newIndex, 0, item);
                this.columns.moveCurrentTo(item);
            }
        }
    }

    // update columns array if the user moves a column
    // (the ng-repeat directive cannot do this)
    draggedColumn(s: wjcGrid.FlexGrid) {
        const columns = new wjcCore.ObservableArray();
        for (let i = 0; i < s.columns.length; i++) {
            columns.push(s.columns[i]);
            console.log('(draggedColumn) ' + s.columns[i]);
        }
        this.columns.sourceCollection = columns;
    }

}
