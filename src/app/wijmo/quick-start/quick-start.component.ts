import {Component, OnInit} from '@angular/core';
import {Hero} from '../../model/hero';
import {HeroService} from '../../model/hero.service';

@Component({
    selector: 'app-quick-start',
    templateUrl: './quick-start.component.html',
    styleUrls: ['./quick-start.component.css']
})
export class QuickStartComponent implements OnInit {
    data: Hero[] = [];

    constructor(private heroService: HeroService) {
    }

    ngOnInit() {
        this.heroService.getHeroes().subscribe(heros => this.data = heros);
    }

}
