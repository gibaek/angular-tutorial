import {Injectable} from '@angular/core';
import * as wjcCore from 'wijmo/wijmo';

@Injectable()
export class DataService {

// data used to generate random items
    getData(count: number): Promise<wjcCore.ObservableArray> {
        const countries = 'US,Germany,UK,Japan,Italy,Greece'.split(','),
            data = new wjcCore.ObservableArray();
        for (let i = 0; i < count; i++) {
            data.push({
                id: i,
                country: countries[i % countries.length],
                date: new Date(2014, i % 12, i % 28),
                amount: Math.random() * 10000,
                active: false
            });
        }
        return Promise.resolve(data);
    }
    getExampleData (count: number): any[] {
        const data = [],
            countries = 'US,Germany,UK,Japan,Italy,Greece,Spain,Canada,Australia,China,Austria'.split(','),
            products = 'Widget,Gadget,Doohickey'.split(','),
            colors = 'Black,White,Red,Green,Blue'.split(','),
            dt = new Date();
        for (let i = 0; i < count; i++) {
            data.push({
                id: i,
                date: new Date(dt.getFullYear(), i % 12, 25, i % 24, i % 60, i % 60),
                country: countries[Math.floor(Math.random() * countries.length)],
                product: products[Math.floor(Math.random() * products.length)],
                color: colors[Math.floor(Math.random() * colors.length)],
                amount: Math.random() * 10000 - 5000,
                discount: Math.random() / 4,
                active: i % 4 === 0,
            });
        }
        return data;
    }

    // getData(count: number): Promise<wjcCore.ObservableArray> {
    //     const countries = 'US,Germany,UK,Japan,Italy,Greece'.split(','),
    //         data = new wjcCore.ObservableArray();
    //     for (let i = 0; i < count; i++) {
    //         data.push({
    //             id: i,
    //             country: countries[i % countries.length],
    //             downloads: Math.round(Math.random() * 20000),
    //             sales: Math.random() * 10000,
    //             expenses: Math.random() * 5000
    //         });
    //     }
    //     return Promise.resolve(data);
    // }

    getTreeData(): Promise<any> {
        const data = [
            {
                name: '\u266B Adriane Simione', items: [
                {
                    name: '\u266A Intelligible Sky', items: [
                    {name: 'Theories', length: '2:02'},
                    {name: 'Giant Eyes', length: '3:29'},
                    {name: 'Jovian Moons', length: '1:02'},
                    {name: 'Open Minds', length: '2:41'},
                    {name: 'Spacetronic Eyes', length: '3:41'}]
                }
            ]
            },
            {
                name: '\u266B Amy Winehouse', items: [
                {
                    name: '\u266A Back to Black', items: [
                    {name: 'Addicted', length: '1:34'},
                    {name: 'He Can Only Hold Her', length: '2:22'},
                    {name: 'Some Unholy War', length: '2:21'},
                    {name: 'Wake Up Alone', length: '3:43'},
                    {name: 'Tears Dry On Their Own', length: '1:25'}]
                }
            ]
            }];
        return Promise.resolve(data);
        // more hierarchical data...
    }

    getAnyData(): any[] {
        const countries = 'US,Germany,UK,Japan,Italy,Greece'.split(','),
            data = [];
        for (let i = 0; i < 30; i++) {
            data.push({
                id: i,
                date: new Date(2015, Math.floor(i / countries.length) % 12, (Math.floor(i / countries.length) + 1) % 28),
                country: countries[i % countries.length],
                countryMapped: i % countries.length,
                downloads: Math.round(Math.random() * 20000),
                sales: Math.random() * 10000,
                expenses: Math.random() * 5000,
                checked: i % 9 === 0
            });
        }
        return data;
    }

    getCv(data: any[]): wjcCore.CollectionView {
        const dataCv = new wjcCore.CollectionView(data);
        dataCv.sortDescriptions.push(new wjcCore.SortDescription('date', true));
        dataCv.groupDescriptions.push(new wjcCore.PropertyGroupDescription('country'));
        return dataCv;
    }
}

