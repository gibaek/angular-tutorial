import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {WijmoComponent} from './wijmo.component';
import {QuickStartComponent} from './quick-start/quick-start.component';
import {WjGridModule} from 'wijmo/wijmo.angular2.grid';
import {WjInputModule} from 'wijmo/wijmo.angular2.input';
import {WjChartModule} from 'wijmo/wijmo.angular2.chart';
import {WjCoreModule} from 'wijmo/wijmo.angular2.core';
import {FlexGridComponent} from './flex-grid/flex-grid.component';
import {DataService} from './data.service';
import {CellTemplateComponent} from './cell-template/cell-template.component';
import {SaveColumnLayoutComponent} from './save-column-layout/save-column-layout.component';
import {DynamicColumnComponent} from './dynamic-column/dynamic-column.component';
import {WjGridDetailModule} from 'wijmo/wijmo.angular2.grid.detail';


const routes: Routes = [
    {
        path: 'wijmo', component: WijmoComponent, children: [
        {path: 'quick-start', component: QuickStartComponent},
        {path: 'flex-grid', component: FlexGridComponent},
        {path: 'cell-template', component: CellTemplateComponent},
        {path: 'save-column-layout', component: SaveColumnLayoutComponent},
        {path: 'dynamic-column', component: DynamicColumnComponent},
        {path: '', redirectTo: 'quick-start', pathMatch: 'full'},
    ]
    },
];

@NgModule({
    declarations: [
        WijmoComponent,
        QuickStartComponent,
        FlexGridComponent,
        CellTemplateComponent,
        SaveColumnLayoutComponent,
        DynamicColumnComponent
        ],
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        WjGridModule,
        WjInputModule,
        WjChartModule,
        WjCoreModule,
        WjGridDetailModule
    ],
    providers: [DataService]
})
export class WijmoModule {
}
