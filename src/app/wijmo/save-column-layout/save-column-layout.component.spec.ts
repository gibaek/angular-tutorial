import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SaveColumnLayoutComponent} from './save-column-layout.component';

describe('SaveColumnLayoutComponent', () => {
    let component: SaveColumnLayoutComponent;
    let fixture: ComponentFixture<SaveColumnLayoutComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SaveColumnLayoutComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SaveColumnLayoutComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
