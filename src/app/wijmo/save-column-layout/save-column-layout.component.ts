import {Component, OnInit, ViewChild} from '@angular/core';
import * as wjcCore from 'wijmo/wijmo';

@Component({
    selector: 'app-save-column-layout',
    templateUrl: './save-column-layout.component.html',
    styleUrls: ['./save-column-layout.component.css'],
})
export class SaveColumnLayoutComponent implements OnInit {

    dataSource: any;
    @ViewChild('flex') flex: any;
    constructor() {
        const data = new wjcCore.ObservableArray();
        const countries = 'US,Germany,UK,Japan,Italy,Greece'.split(',');

        for (let i = 0; i < 20; i++) {
            data.push({
                country: countries[i % countries.length],
                downloads: Math.round(Math.random() * 20000),
                sales: Math.random() * 10000,
                expenses: Math.random() * 5000,
            });

            this.dataSource = new wjcCore.CollectionView(data);
        }
    }

    ngOnInit(): void {
    }

    initGrid(flex) {
        flex.initialize({
            columns: [
                {binding: 'country', header: 'Country', minWidth: 70},
                {binding: 'sales', header: 'Sales', minWidth: 70},
                {binding: 'expenses', header: 'Expenses', minWidth: 70},
                {binding: 'downloads', header: 'Downloads', minWidth: 70}
            ]
        });
        this.loadColumnLayout();
        this.loadSort();
    }

    saveColumnLayout() {
        if (localStorage) {
            localStorage['columns'] = this.flex.columnLayout;
        }
    }

    loadColumnLayout() {
        if (localStorage) {
            if (localStorage['columns']) {
                this.flex.autoGenerateColumns = false;
                this.flex.columnLayout = localStorage['columns'];
                console.log('** Loaded layout: ' + this.flex.columnLayout);
            }
        }
    }

    loadSort() {
        if (localStorage['sort']) {
            const sortStates = JSON.parse(localStorage['sort']);
            for (const sortState of sortStates ) {
                console.log('load sort ' + sortState.property + ', ' + sortState.ascending);
                const sd = new wjcCore.SortDescription(sortState.property, sortState.ascending);
                this.flex.collectionView.sortDescriptions.push(sd);
            }
        }
    }

    saveSort() {
        console.log('** Saved layout: ' + this.flex.columnLayout);
        const sortStates: any[] = [];

        for (const description of this.flex.collectionView.sortDescriptions) {
            console.log('description');
            console.log(description);
            sortStates.push({
                property: description.property,
                ascending: description.ascending,
            });
        }
        console.log('** Saved Sort Sate: ' + JSON.stringify(sortStates));
        localStorage['sort'] = JSON.stringify(sortStates);
    }
}
