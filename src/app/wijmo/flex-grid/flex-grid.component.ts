import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import * as wjcCore from 'wijmo/wijmo';
import * as wjcGrid from 'wijmo/wijmo.grid';

@Component({
    selector: 'app-flex-grid',
    templateUrl: './flex-grid.component.html',
    styleUrls: ['./flex-grid.component.css']
})
export class FlexGridComponent implements OnInit {
    data: wjcCore.CollectionView;
    treeData: [{}];

    selectionMode = 'CellRange';

    private _groupBy = '';
    private _filter = '';
    private _toFilter: any;

    constructor(private dataSvc: DataService) {
        this.dataSvc.getData(100).then(data => this.data = new wjcCore.CollectionView(data));
        this.dataSvc.getTreeData().then(data => this.treeData = data);
    }

    ngOnInit() {
        this.data.pageSize = 20;
        this.data.filter = this._filterFunction.bind(this);
    }

    /* Cell Freezing */
    toggleFreeze(flex: wjcGrid.FlexGrid) {
        if (flex) {
            console.log(flex.columnLayout);
            const frozenCount = flex.frozenRows === 0 ? 2 : 0;
            flex.frozenRows = frozenCount;
            flex.frozenColumns = frozenCount;
        }
    }

    /* Grouping */
    get groupBy(): string {
        return this._groupBy;
    }

    set groupBy(value: string) {
        if (this._groupBy !== value) {
            this._groupBy = value;
            this._applyGroupBy();
        }
    }

    private _applyGroupBy() {
        const cv = this.data;
        cv.beginUpdate();
        cv.groupDescriptions.clear();
        if (this.groupBy) {
            const groupNames = this.groupBy.split(',');
            for (let i = 0; i < groupNames.length; i++) {
                const groupName = groupNames[i];
                if (groupName === 'date') { // group dates by year
                    const groupDesc = new wjcCore.PropertyGroupDescription(groupName, function (item, prop) {
                        return item.date.getFullYear();
                    });
                    cv.groupDescriptions.push(groupDesc);
                } else if (groupName === 'amount') { // group amounts in ranges
                    const groupDesc = new wjcCore.PropertyGroupDescription(groupName, function (item, prop) {
                        return item.amount >= 5000 ? '> 5,000' : item.amount >= 500 ? '500 to 5,000' : '< 500';
                    });
                    cv.groupDescriptions.push(groupDesc);
                } else { // group everything else by value
                    const groupDesc = new wjcCore.PropertyGroupDescription(groupName);
                    cv.groupDescriptions.push(groupDesc);
                }
            }
            cv.refresh();
        }
        cv.endUpdate();
    }


    /* Filtering */
    get filter(): string {
        return this._filter;
    }

    set filter(value: string) {
        if (this._filter !== value) {
            this._filter = value;
            if (this._toFilter) {
                clearTimeout(this._toFilter);
            }
            this._toFilter = setTimeout(() => {
                this.data.refresh();
            }, 500);
        }
    }

    private _filterFunction(item: any) {
        if (this._filter) {
            return item.country.toLowerCase().indexOf(this._filter.toLowerCase()) > -1;
        }
        return true;
    }

    getAmountColor(amount: number) {
        if (amount < 500) {
            return 'darkred';
        }
        if (amount < 2500) {
            return 'black';
        }
        return 'darkgreen';
    }
}
