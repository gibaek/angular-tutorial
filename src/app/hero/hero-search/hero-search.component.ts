import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Hero} from '../../model/hero';
import {Subject} from 'rxjs/Subject';
import {HeroService} from '../../model/hero.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-hero-search',
    templateUrl: './hero-search.component.html',
    styleUrls: ['./hero-search.component.css']
})
export class HeroSearchComponent implements OnInit {

    heroes: Observable<Hero[]>;
    private serchTerms = new Subject<string>();

    constructor(private heroService: HeroService, private router: Router) {
    }

    search(term: string): void {
        this.serchTerms.next(term);
    }

    ngOnInit(): void {
        this.heroes = this.serchTerms
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap(term => term ? this.heroService.search(term)
            : Observable.of<Hero[]>([]))
            .catch(error => {
                console.log(error);
                return Observable.of<Hero[]>([]);
            });
    }

    gotoDetail(hero: Hero): void {
        const link = ['/detail', hero.id];
        this.router.navigate(link);
    }

}
