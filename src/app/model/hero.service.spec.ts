import {inject, TestBed} from '@angular/core/testing';

import {HeroService} from './hero.service';

fdescribe('HeroService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [HeroService],
        });
    });

    it('should be created', inject([HeroService], (service: HeroService) => {
        expect(service).toBeTruthy();
    }));
});

