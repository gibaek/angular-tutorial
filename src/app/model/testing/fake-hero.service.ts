// re-export for tester convenience

import {FakeHero} from './fake-hero';
import {HeroService} from '../hero.service';

export {FakeHero} from './fake-hero';
export {HeroService} from '../hero.service';

export const HEROES: FakeHero[] = [
    new FakeHero(41, 'Bob'),
    new FakeHero(42, 'Carol'),
    new FakeHero(43, 'Ted'),
    new FakeHero(44, 'Alice'),
    new FakeHero(45, 'Speedy'),
    new FakeHero(46, 'Stealthy')
];

export class FakeHeroService {
}
