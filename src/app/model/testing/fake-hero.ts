export class FakeHero {
    id: number;
    name: string;

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }

    clone() {
        return new FakeHero(this.id, this.name);
    }
}
