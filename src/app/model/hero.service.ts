import {Injectable} from '@angular/core';
import {Hero} from './hero';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class HeroService {

    private heroesUrl = 'api/heroes';
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) {
    }

    search(term: string): Observable<Hero[]> {
        return this.http
            .get(`api/heroes/?name=${term}`)
            .map(res => res.json().data as Hero[]);
    }

    getHeroes(): Observable<Hero[]> {
        return this.http.get(this.heroesUrl)
            .map(response => response.json().data as Hero[])
            .catch(this.handleError);
    }

    private handleError(error: any): Observable<any> {
        console.error('An error occurred', error);
        return Observable.throw(error.message || error);
    }

    getHero(id: number): Observable<Hero> {
        const url = `${this.heroesUrl}/${id}`;
        return this.http.get(url)
            .map(response => response.json().data as Hero)
            .catch(this.handleError);
    }

    updateHero(hero: Hero): Promise<Hero> {
        const url = `${this.heroesUrl}/${hero.id}`;
        return this.http
            .put(url, JSON.stringify(hero))
            .toPromise()
            .then(() => hero)
            .catch(this.handleError);
    }

    create(name: string): Promise<Hero> {
        return this.http
            .post(this.heroesUrl, JSON.stringify({name: name}))
            .toPromise()
            .then(res => res.json().data as Hero)
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        const url = `${this.heroesUrl}/${id}`;
        return this.http.delete(url)
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
}
