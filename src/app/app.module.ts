import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BannerComponent} from './banner/banner.component';
import {HeroDetailComponent} from './hero/hero-detail/hero-detail.component';
import {HeroListComponent} from './hero/hero-list/hero-list.component';
import {AppRoutingModule} from './app-routing.module';
import {HeroService} from './model/hero.service';
import {WelcomeComponent} from './welcome/welcome.component';
import {UserService} from './model/user.service';
import {TwainService} from './shared/twain/twain.service';
import {SharedModule} from './shared/shared.module';
import {DashboardModule} from './dashboard/dashboard.module';
import {WijmoModule} from './wijmo/wijmo.module';
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './model/in-memory-data.service';

@NgModule({
    declarations: [
        AppComponent,
        BannerComponent,
        HeroDetailComponent,
        HeroListComponent,
        WelcomeComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        SharedModule,
        DashboardModule,
        WijmoModule,
        InMemoryWebApiModule.forRoot(InMemoryDataService)
    ],
    providers: [HeroService, UserService, TwainService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
