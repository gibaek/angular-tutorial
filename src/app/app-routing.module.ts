import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {HeroDetailComponent} from './hero/hero-detail/hero-detail.component';
import {HeroListComponent} from './hero/hero-list/hero-list.component';
import {QuickStartComponent} from './wijmo/quick-start/quick-start.component';
import {WijmoComponent} from './wijmo/wijmo.component';

const routes: Routes = [
    {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
    {path: 'detail/:id', component: HeroDetailComponent},
    {path: 'heroes', component: HeroListComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
