import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {TwainComponent} from './twain/twain.component';
import {HttpModule} from '@angular/http';

@NgModule({
    imports: [
        CommonModule, FormsModule, HttpModule
    ],
    exports: [
        CommonModule, FormsModule, TwainComponent, HttpModule
    ],
    declarations: [TwainComponent]
})
export class SharedModule {
}
